#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import json
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Bot
from peewee import *

db = SqliteDatabase('reports.db')


class Report(Model):
    name = CharField()
    userid = IntegerField()

    class Meta:
        database = db


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


def getconfig(mode):
    with open("config/config.json", mode) as config_file:
        config_raw = config_file.read()
        configuration = json.loads(config_raw)
        return configuration


bot = Bot(getconfig('r')['token'])


def start(update, context):
    """Send a message when the command /start is issued."""
    reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton("Antworten", callback_data="fff")]])
    update.message.reply_text(
        'Hi! Bitte sende mir deine Anfrage / Meldung. Tipp: Wenn du willst,'
        'das wir dich kontaktieren können, stelle bitt einen Username ein', reply_markup=reply_markup
    )


def help(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Hallo, ich bin der offizille Meldungs / Anfrage Bot von FridaysForFuture DE')


def privacy(update, context):
    update.message.reply_text("Welche Daten werden erhoben? Username bzw Name und Vorname\n"
                              "Was passiert mit meinen Daten? Deine Daten werden ausschließlich "
                              "an die jeweilige AG weitergeleitet.")


message_query = {}


def handleText(update, context):
    """Echo the user message."""
    keyboard = []
    topics = getconfig("r")["topics"]
    buffer = []

    for topic, chatid in topics.items():
        buffer.append(InlineKeyboardButton(topic, callback_data=chatid))

        if len(buffer) >= 2:
            keyboard.append(buffer)
            buffer = []

    if len(buffer) > 0:
        keyboard.append(buffer)

    reply_markup = InlineKeyboardMarkup(keyboard)
    message_query[update.effective_user.id] = update.message.text
    update.message.reply_text('Bitte wähle aus, auf was sich deine Meldung / Anfrage bezieht:',
                              reply_markup=reply_markup)


def sendmsgtogroup(update, context):
    query = update.callback_query
    reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton("Antworten", callback_data="fff")]])
    bot.send_message(query.data, update.effective_user.name + ":\n" + message_query[update.effective_user.id],
                     reply_markup=reply_markup)
    del message_query[update.effective_user.id]
    query.edit_message_text(text="Vielen Dank! Deine Anfrage wird von der Zuständigen AG bearbeitet.")


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    """Start the bot."""
    updater = Updater(getconfig('r')["token"], use_context=True)

    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("datenschutz", privacy))
    dp.add_handler(CallbackQueryHandler(sendmsgtogroup))

    dp.add_handler(MessageHandler(Filters.text, handleText))

    dp.add_error_handler(error)

    updater.start_polling()
    updater.idle()


main()
